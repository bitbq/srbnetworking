#import "SRBNetworkingImageRequest.h"

static BOOL __imageRequestShouldCacheImages = YES;

@implementation SRBNetworkingImageRequest

-(id) formattedResponseDataFromResultsData: (NSData *) resultsData
{
	return [UIImage imageWithData: resultsData];
}

+(void) setShouldCacheImages: (BOOL) shouldCacheImages
{
	__imageRequestShouldCacheImages = shouldCacheImages;
}

@end
